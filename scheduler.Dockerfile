FROM composer AS build

WORKDIR /build

ADD . .

RUN composer install --no-dev --ignore-platform-reqs

FROM php:7.4-alpine

RUN set -ex && apk --no-cache add \
    postgresql-dev

RUN docker-php-ext-install \
    pdo_pgsql \
    sockets \
    bcmath

RUN apk add supervisor

COPY /deploy/supervisord.conf /etc/supervisord.conf

WORKDIR /app

COPY --from=build /build /app

RUN chown -R 1000:1000 /app
RUN chmod -R 755 /app/storage

ENTRYPOINT [ "supervisord" ]
