<?php

use App\Book;
use App\BookContent;
use App\Services\MyBookService;
use Faker\Factory;
use Laravel\Lumen\Testing\DatabaseMigrations;

class MyBookServiceTest extends TestCase
{
    use DatabaseMigrations;

    protected $my_book_service;

    protected $faker;

    public function setUp(): void
    {
        parent::setUp();

        $this->faker = Factory::create();
        $this->my_book_service = new MyBookService;

        $seeder = app('DatabaseSeeder');
        $seeder->call('PublishersTableSeeder');
        $seeder->call('AuthorsTableSeeder');
        $seeder->call('BooksTableSeeder');
    }

    public function testFindMyBookService()
    {
        $book = $this->my_book_service->find(1, 1);

        $this->assertInstanceOf(Book::class, $book);
    }

    public function testReadMyBookService()
    {
        $content = $this->my_book_service->read(1, 1, 1);

        $this->assertInstanceOf(BookContent::class, $content);
    }
}
