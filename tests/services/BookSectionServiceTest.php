<?php

use App\BookSection;
use App\Services\BookSectionService;
use Faker\Factory;
use Laravel\Lumen\Testing\DatabaseMigrations;

class BookSectionServiceTest extends TestCase
{
    use DatabaseMigrations;

    protected $book_section_service;

    protected $faker;

    public function setUp(): void
    {
        parent::setUp();

        $this->faker = Factory::create();
        $this->book_section_service = new BookSectionService;

        $seeder = app('DatabaseSeeder');
        $seeder->call('PublishersTableSeeder');
        $seeder->call('AuthorsTableSeeder');
        $seeder->call('BooksTableSeeder');
    }

    public function testGetBookSectionService()
    {
        $data = $this->book_section_service->get();

        $this->assertTrue(count($data) > 0);
    }

    public function testFindBookSection()
    {
        $expected_id = 1;

        $actual = $this->book_section_service->find($expected_id);

        $this->assertEquals($expected_id, $actual->id);
    }

    public function testCreateBookSectionService()
    {
        $data = [
            'book_id' => 1,
            'title' => $this->faker->name,
            'description' => $this->faker->sentence(6),
        ];

        $section = $this->book_section_service->create($data);

        $this->assertInstanceOf(BookSection::class, $section);
        $this->assertEquals($data['title'], $section->title);
        $this->assertEquals($data['description'], $section->description);
        $this->assertEquals($data['book_id'], $section->book->id);
    }

    public function testUpdateBookSectionService()
    {
        $excpected_id = 1;
        $data = [
            'book_id' => 1,
            'title' => $this->faker->name,
            'description' => $this->faker->sentence(6),
        ];

        $section = $this->book_section_service->update($excpected_id, $data);

        $this->assertInstanceOf(BookSection::class, $section);
        $this->assertEquals($excpected_id, $section->id);
        $this->assertEquals($data['title'], $section->title);
        $this->assertEquals($data['description'], $section->description);
        $this->assertEquals($data['book_id'], $section->book->id);
    }
}
