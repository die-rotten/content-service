<?php

use App\Services\QuestionService;
use App\Question;
use Laravel\Lumen\Testing\DatabaseMigrations;

class QuestionServiceTest extends TestCase
{
    use DatabaseMigrations;

    protected $question_service_test;

    public function setUp(): void
    {
        parent::setUp();

        $this->question_service_test = new QuestionService();

        $seeder = app('DatabaseSeeder');
        $seeder->call('PublishersTableSeeder');
        $seeder->call('AuthorsTableSeeder');
        $seeder->call('BooksTableSeeder');
        $seeder->call('QuestionsTableSeeder');
        $seeder->call('PackagesTableSeeder');
        $seeder->call('PackagesUsersTableSeeder');
    }

    public function testGetByContentAndUserQuestionService()
    {
        $questions = $this->question_service_test->getByContentAndUser(1, 1);

        $this->assertTrue(count($questions) > 0);
    }

    public function testCreateQuestionService()
    {
        $data = [
            'user_id' => 1,
            'content_id' => 1,
            'title' => 'test',
            'description' => 'test',
        ];

        $question = $this->question_service_test->create($data);

        $this->assertInstanceOf(Question::class, $question);
        $this->assertEquals($data['user_id'], $question->user_id);
        $this->assertEquals($data['content_id'], $question->content_id);
        $this->assertEquals($data['title'], $question->title);
        $this->assertEquals($data['description'], $question->description);
    }
}
