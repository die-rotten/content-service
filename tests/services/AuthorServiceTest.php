<?php

use App\Author;
use App\Services\AuthorService;
use Illuminate\Support\Facades\Event;
use Faker\Factory;
use Laravel\Lumen\Testing\DatabaseMigrations;

class AuthorServiceTest extends TestCase
{
    use DatabaseMigrations;
    
    protected $author_service;

    protected $faker;

    public function setUp(): void
    {
        parent::setUp();

        $this->faker = Factory::create();
        $this->author_service = new AuthorService;

        $seeder = app('DatabaseSeeder');
        $seeder->call('PublishersTableSeeder');
        $seeder->call('AuthorsTableSeeder');
    }

    public function testGetAuthorService()
    {
        $data = $this->author_service->get();

        $this->assertTrue(count($data) > 0);
    }

    public function testFindAuthorService()
    {
        $expected_id = 1;
        $data = $this->author_service->find($expected_id);

        $this->assertInstanceOf(Author::class, $data);
    }

    public function testCreateAuthorService()
    {
        Event::fake();

        $data = [
            'first_name' => $this->faker->name,
            'last_name' => $this->faker->name,
            'publisher_id' => 1,
            'email' => $this->faker->name,
            'title' => $this->faker->name,
            'status' => true,
            'phone' => $this->faker->phoneNumber,
        ];

        $author = $this->author_service->create($data);

        $this->assertInstanceOf(Author::class, $author);
        $this->assertEquals($data['first_name'], $author->first_name);
        $this->assertEquals($data['last_name'], $author->last_name);
        $this->assertEquals($data['email'], $author->email);
        $this->assertEquals($data['title'], $author->title);
        $this->assertEquals($data['status'], $author->status);
        $this->assertEquals($data['phone'], $author->phone);
    }

    public function testUpdateAuthorService()
    {
        $expected_id = 1;
        $data = [
            'first_name' => $this->faker->name,
            'last_name' => $this->faker->name,
            'publisher_id' => 1,
            'email' => $this->faker->name,
            'title' => $this->faker->name,
            'status' => true,
            'phone' => $this->faker->phoneNumber,
        ];

        $author = $this->author_service->update($expected_id, $data);

        $this->assertInstanceOf(Author::class, $author);
        $this->assertEquals($data['first_name'], $author->first_name);
        $this->assertEquals($data['last_name'], $author->last_name);
        $this->assertEquals($data['email'], $author->email);
        $this->assertEquals($data['title'], $author->title);
        $this->assertEquals($data['status'], $author->status);
        $this->assertEquals($data['phone'], $author->phone);
    }

    public function testDeleteAuthorService()
    {
        $expected_id = 1;
        $data = $this->author_service->delete($expected_id);

        $this->assertTrue($data);
    }
}
