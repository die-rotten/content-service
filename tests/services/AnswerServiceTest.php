<?php

use App\Answer;
use App\Services\AnswerService;
use Laravel\Lumen\Testing\DatabaseMigrations;

class AnswerServiceTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();

        $seeder = app('DatabaseSeeder');
        $seeder->call('PublishersTableSeeder');
        $seeder->call('AuthorsTableSeeder');
        $seeder->call('BooksTableSeeder');
        $seeder->call('QuestionsTableSeeder');
    }

    public function testCreateAnswerService()
    {
        $answer_service_test = new AnswerService;
        $answer = $answer_service_test->create([
            'user_id' => 1,
            'email' => 'ustabdulkhodir@gmail.com',
            'question_id' => 1,
            'title' => 'test',
            'description' => 'test',
        ]);

        $this->assertInstanceOf(Answer::class, $answer);
    }
}
