<?php

use App\Package;
use App\Services\PackageService;
use Faker\Factory;
use Laravel\Lumen\Testing\DatabaseMigrations;

class PackageServiceTest extends TestCase
{
    use DatabaseMigrations;

    protected $test_package_service;

    protected $faker;

    public function setUp(): void
    {
        parent::setUp();

        $this->test_package_service = new PackageService;
        $this->faker = Factory::create();

        $seeder = app('DatabaseSeeder');
        $seeder->call('PublishersTableSeeder');
        $seeder->call('AuthorsTableSeeder');
        $seeder->call('BooksTableSeeder');
        $seeder->call('PackagesTableSeeder');
        $seeder->call('PackagesUsersTableSeeder');
    }

    public function testGetPackageService()
    {
        $data = $this->test_package_service->get();

        $this->assertTrue(count($data) > 0);
    }

    public function testGetByUserPackageService()
    {
        $data = $this->test_package_service->getByUser(1);

        $this->assertTrue(count($data) > 0);
    }

    public function testFindByUserPackageService()
    {
        $data = $this->test_package_service->findByUser(1, 1);

        $this->assertInstanceOf(Package::class, $data);
    }

    public function testValidatePackagePackageService()
    {
        $response = $this->test_package_service->validatePackage(1, 1);

        $this->assertTrue($response);
    }

    public function testFindPackageService()
    {
        $data = $this->test_package_service->find(1);

        $this->assertInstanceOf(Package::class, $data);
        $this->assertEquals(1, $data->id);
    }

    public function testCreatePackageService()
    {
        $data = [
            'title' => $this->faker->name,
            'description' => $this->faker->sentence(3),
            'price' => 50000,
            'discount' => 0,
            'book_ids' => [1],
        ];

        $package = $this->test_package_service->create($data);
        
        $this->assertInstanceOf(Package::class, $package);
        $this->assertEquals($data['title'], $package->title);
    }

    public function testStoreUserPackagePackageService()
    {
        $package = $this->test_package_service->storeUserPackage(1, [1]);

        $this->assertTrue($package);
    }

    public function testUpdatePackageService()
    {
        $data = [
            'title' => $this->faker->name,
            'description' => $this->faker->sentence(3),
            'price' => 50000,
            'discount' => 0,
            'book_ids' => [1],
        ];

        $package = $this->test_package_service->update(1, $data);

        $this->assertInstanceOf(Package::class, $package);
        $this->assertEquals($data['title'], $package->title);
    }
}
