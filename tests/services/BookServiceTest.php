<?php

use App\Book;
use App\BookSection;
use App\Services\BookService;
use Faker\Factory;
use Laravel\Lumen\Testing\DatabaseMigrations;

class BookServiceTest extends TestCase
{
    use DatabaseMigrations;

    protected $book_service;

    protected $faker;

    public function setUp(): void
    {
        parent::setUp();

        $this->faker = Factory::create();
        $this->book_service = new BookService;

        $seeder = app('DatabaseSeeder');
        $seeder->call('PublishersTableSeeder');
        $seeder->call('AuthorsTableSeeder');
        $seeder->call('BooksTableSeeder');
    }

    public function testGetBookService()
    {
        $data = $this->book_service->get();

        $this->assertTrue(count($data) > 0);
    }

    public function testFindBookService()
    {
        $expected_id = 1;
        $data = $this->book_service->find($expected_id);

        $this->assertEquals($expected_id, $data['id']);
        $this->assertInstanceOf(Book::class, $data);
    }

    public function testGetSectionBookService()
    {
        $expected_id = 1;
        $data = $this->book_service->getSections($expected_id);

        $this->assertTrue(count($data) > 0);
        $this->assertInstanceOf(BookSection::class, $data[0]);
    }

    public function testCreateBookService()
    {
        $data = [
            'title' => $this->faker->name,
            'description' => $this->faker->sentence(6),
            'price' => 4000,
            'author_ids' => [1],
        ];

        $book = $this->book_service->create($data);
        $author = $book->authors()->first();

        $this->assertInstanceOf(Book::class, $book);
        $this->assertEquals($data['title'], $book->title);
        $this->assertEquals($data['description'], $book->description);
        $this->assertEquals($data['price'], $book->price);
        $this->assertEquals($data['author_ids'][0], $author->id);
    }

    public function testUpdateBookService()
    {
        $expected_id = 1;

        $data = [
            'title' => $this->faker->name,
            'description' => $this->faker->sentence(6),
            'price' => 4000,
            'author_ids' => [1],
        ];

        $book = $this->book_service->update($expected_id, $data);
        $author = $book->authors()->first();

        $this->assertInstanceOf(Book::class, $book);
        $this->assertEquals($data['title'], $book->title);
        $this->assertEquals($data['description'], $book->description);
        $this->assertEquals($data['price'], $book->price);
        $this->assertEquals($data['author_ids'][0], $author->id);
    }
}
