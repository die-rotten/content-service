<?php

use App\Publisher;
use App\Services\PublisherService;
use Faker\Factory;
use Laravel\Lumen\Testing\DatabaseMigrations;

class PublisherServiceTest extends TestCase
{
    use DatabaseMigrations;

    protected $publisher_service;

    protected $faker;

    public function setUp() :void
    {
        parent::setUp();

        $this->faker = Factory::create();
        $this->publisher_service = new PublisherService;

        $seeder = app('DatabaseSeeder');
        $seeder->call('PublishersTableSeeder');
    }

    public function testGetPublisherService()
    {
        $data = $this->publisher_service->get();

        $this->assertTrue(count($data) > 0);
    }

    public function testFindPublisherService()
    {
        $expected_id = 1;
        $data = $this->publisher_service->find($expected_id);

        $this->assertInstanceOf(Publisher::class, $data);
        $this->assertEquals($expected_id, $data->id);
    }

    public function testCreatePublisherService()
    {
        $data = [
            'name' => $this->faker->name,
            'address' => $this->faker->address,
            'phone' => $this->faker->phoneNumber,
        ];

        $publisher = $this->publisher_service->create($data);

        $this->assertInstanceOf(Publisher::class, $publisher);
        $this->assertEquals($data['name'], $publisher->name);
        $this->assertEquals($data['phone'], $publisher->phone);
        $this->assertEquals($data['address'], $publisher->address);
    }

    public function testUpdatePublisher()
    {
        $expected_id = 1;
        $data = [
            'name' => $this->faker->name,
            'address' => $this->faker->address,
            'phone' => $this->faker->phoneNumber,
        ];

        $publisher = $this->publisher_service->update($expected_id, $data);
        $this->assertInstanceOf(Publisher::class, $publisher);
        $this->assertEquals($data['name'], $publisher->name);
        $this->assertEquals($data['phone'], $publisher->phone);
        $this->assertEquals($data['address'], $publisher->address);
    }

    public function testDeletePublisherService()
    {
        $expected_id = 1;
        $data = $this->publisher_service->delete($expected_id);

        $this->assertTrue($data);
    }
}
