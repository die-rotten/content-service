<?php

use App\BookContent;
use App\Services\BookContentService;
use Faker\Factory;
use Laravel\Lumen\Testing\DatabaseMigrations;

class BookContentServiceTest extends TestCase
{
    use DatabaseMigrations;

    protected $book_content_service;

    protected $faker;

    public function setUp(): void
    {
        parent::setUp();

        $this->faker = Factory::create();
        $this->book_content_service = new BookContentService;

        $seeder = app('DatabaseSeeder');
        $seeder->call('PublishersTableSeeder');
        $seeder->call('AuthorsTableSeeder');
        $seeder->call('BooksTableSeeder');
    }

    public function testFindBookContentService()
    {
        $expected_id = 1;
        $content = $this->book_content_service->find($expected_id);

        $this->assertInstanceOf(BookContent::class, $content);
        $this->assertEquals($expected_id, $content->id);
    }

    public function testCreateBookContentService()
    {
        $data = [
            'book_section_id' => 1,
            'title' => $this->faker->name,
            'content' => json_encode([
                'content' => '<b>' . $this->faker->sentence(6) . '</b>'
            ]),
            'type' => 'Text',
            'duration' => $this->faker->randomDigit,
            'duration_unit' => 'minute',
        ];

        $content = $this->book_content_service->create($data);

        $this->assertInstanceOf(BookContent::class, $content);
        $this->assertEquals($data['title'], $content->title);
        $this->assertEquals($data['content'], $content->content);
        $this->assertEquals($data['duration'], $content->duration);
        $this->assertEquals($data['duration_unit'], $content->duration_unit);
    }

    public function testUpdateBookContentService()
    {
        $expected_id = 1;
        $data = [
            'book_section_id' => 1,
            'title' => $this->faker->name,
            'content' => json_encode([
                'content' => '<b>' . $this->faker->sentence(6) . '</b>'
            ]),
            'type' => 'Text',
            'duration' => $this->faker->randomDigit,
            'duration_unit' => 'minute',
        ];

        $content = $this->book_content_service->update($expected_id, $data);
        $this->assertInstanceOf(BookContent::class, $content);
        $this->assertEquals($data['title'], $content->title);
        $this->assertEquals($data['content'], $content->content);
        $this->assertEquals($data['duration'], $content->duration);
        $this->assertEquals($data['duration_unit'], $content->duration_unit);
    }
}
