<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', 'IndexController@index');

// For Publisher module.
$router->get('/publishers', 'PublisherController@get');
$router->get('/publishers/{id}', 'PublisherController@find');
$router->post('/publishers', 'PublisherController@store');
$router->patch('/publishers/{id}', 'PublisherController@update');
$router->delete('/publishers/{id}', 'PublisherController@delete');

// For Author module.
$router->get('/authors', 'AuthorController@get');
$router->get('/authors/{id}', 'AuthorController@find');
$router->post('authors', 'AuthorController@store');
$router->patch('/authors/{id}', 'AuthorController@update');
$router->delete('/authors/{id}', 'AuthorController@delete');

// For Book module.
$router->get('/books', 'BookController@get');
$router->get('/books/by-author', [
    'uses' => 'BookController@getByAuthor',
    'middleware' => 'author',
]);
$router->post('/books', 'BookController@store');
$router->get('/books/{id}', 'BookController@find');
$router->patch('/books/{id}', 'BookController@update');
$router->get('/books/{id}/sections', 'BookController@getSections');

// For BookSection module.
$router->get('/book-sections', 'BookSectionController@get');
$router->get('/book-sections/{id}', 'BookSectionController@find');
$router->post('/book-sections', 'BookSectionController@store');
$router->patch('/book-sections/{id}', 'BookSectionController@update');

// For BookContent module.
$router->get('/book-contents/{id}', 'BookContentController@find');
$router->post('/book-contents', 'BookContentController@store');
$router->patch('/book-contents/{id}', 'BookContentController@update');

// For Package module.
$router->get('/packages', 'PackageController@get');
$router->get('/packages/{id}', 'PackageController@find');
$router->get('/packages/{id}/validate', [
    'middleware' => 'auth',
    'uses' => 'PackageController@validatePackage'
]);
$router->post('/packages', 'PackageController@store');
$router->patch('/packages/{id}', 'PackageController@update');
$router->post('/packages/user', 'PackageController@storeUserPackage');
$router->get('/my/packages', [
    'middleware' => 'auth',
    'uses' => 'PackageController@getMyPackage',
]);
$router->get('/my/packages/last-read', [
    'middleware' => 'auth',
    'uses' => 'PackageController@getLastRead',
]);
$router->get('/my/packages/{id}', [
    'middleware' => 'auth',
    'uses' => 'PackageController@findMyPackage',
]);

// For Question module.
$router->get('/questions/for-author', [
    'middleware' => 'author',
    'uses' => 'QuestionController@getForAuthor',
]);

$router->get('/questions/{content_id}', [
    'middleware' => 'auth',
    'uses' => 'QuestionController@get',
]);

$router->post('/questions', [
    'middleware' => 'auth',
    'uses' => 'QuestionController@store',
]);

// For Answer module.
$router->post('/answers', [
    'middleware' => 'author',
    'uses' => 'AnswerController@store',
]);

// For MyBook module.
$router->get('/my/books/latest', [
    'middleware' => 'auth',
    'uses' => 'MyBookController@getLatest',
]);
$router->get('/my/books/{id}', [
    'middleware' => 'auth',
    'uses' => 'MyBookController@find'
]);
$router->get('/my/books/{id}/read/{content_id}', [
    'middleware' => 'auth',
    'uses' => 'MyBookController@read'
]);

// For report module
$router->get('/reports/authors-amount', [
    'middleware' => 'auth',
    'uses' => 'ReportController@getAuthorsAmount',
]);

$router->get('/reports/packages-amount', [
    'middleware' => 'auth',
    'uses' => 'ReportController@getPackagesAmount',
]);

$router->get('/reports/popular-packages', [
    'middleware' => 'auth',
    'uses' => 'ReportController@getMostPopularPackages',
]);
