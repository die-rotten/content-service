<?php

namespace App\Services;

use App\Answer;
use App\Exceptions\AuthorNotAllowedException;
use App\Question;

class AnswerService
{
    /**
     * Create creates answer.
     *
     * @param array $data
     * @return void
     */
    public function create(array $data)
    {
        try {
            $question = Question::find($data['question_id']);
            $authors = $question->content->section->book->authors;

            if (is_null($authors->where('email', $data['email'])->first())) {
                throw new AuthorNotAllowedException();
            }

            return Answer::create($data);
        } catch (\Exception  $e) {
            throw $e;
        }
    }
}
