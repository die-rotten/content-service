<?php

namespace App\Services;

use App\Author;
use App\Book;
use App\BookContent;
use App\Exceptions\NotAllowAddQuestionException;
use App\Question;
use Illuminate\Support\Facades\DB;

class QuestionService
{
    /**
     * Get question by content and user.
     *
     * @param int $content_id
     * @param int $user_id
     * @return void
     */
    public function getByContentAndUser($content_id, $user_id)
    {
        return Question::where([
            'content_id' => $content_id,
            'user_id' => $user_id,
        ])->get();
    }

    /**
     * Create creates new question.
     *
     * @param array $data
     * @return void
     */
    public function create(array $data)
    {
        try {
            $content = BookContent::find($data['content_id']);
            $packageIDs = $content->section->book->packages->pluck('id');

            $found = DB::table('packages_users')
                ->whereIn('package_id', $packageIDs->toArray())
                ->where('user_id', $data['user_id'])
                ->get()
                ->unique();

            if (count($found) === 0) {
                throw new NotAllowAddQuestionException();
            }

            return Question::create($data);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Return all question by an author
     *
     * @param array $author
     * @return void
     */
    public function getForAuthor($author)
    {
        try {
            $author = Author::where('email', $author['email'])->first();
            $books = $author->books;

            $contentIDs = collect($books)->reduce(function ($carry, $book) {
                $items = $book->contents()->pluck('book_contents.id');
                return array_merge($carry, $items->all());
            }, []);

            $questions = Question::with(['content', 'answers'])
                ->whereIn('content_id', $contentIDs)
                ->get();

            return $questions;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
