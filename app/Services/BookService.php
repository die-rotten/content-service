<?php

namespace App\Services;

use App\Author;
use App\Book;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;

class BookService
{
    protected $relationship = [
        'authors',
        'sections',
        'sections.contents'
    ];

    /**
     * Return all books.
     *
     * @return void
     */
    public function get()
    {
        return Book::with($this->relationship)->get();
    }

    /**
     * Returning sections of a book.
     *
     * @param int $id
     * @return void
     */
    public function getSections($id)
    {
        try {
            return Book::findOrFail($id)->sections;
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    /**
     * Return a book.
     *
     * @param id $id
     * @return void
     */
    public function find($id)
    {
        $book = Book::with([
            'sections' => function ($query) {
                $query->orderBy('id');
            },
            'sections.contents' => function ($query) {
                $query->orderBy('id');
            },
            'authors' => function ($query) {
                $query->orderBy('created_at');
            },
        ])->findOrFail($id);

        return $book;
    }

    /**
     * GetByAuthor return all books by an author
     *
     * @param array $author
     * @return void
     */
    public function getByAuthor($author)
    {
        try {
            $author = Author::where('email', $author['email'])->first();
            return $author->books;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Store a book to database.
     *
     * @param array $data
     * @return void
     */
    public function create($data)
    {
        try {
            $book = DB::transaction(function () use ($data) {
                $book = Book::create($data);
                $book->authors()->sync($data['author_ids']);

                return $book;
            });

            return $book;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Update a book to database.
     *
     * @param int $id
     * @param array $data
     * @return void
     */
    public function update($id, array $data)
    {
        try {
            return DB::transaction(function () use ($id, $data) {
                $book = $this->find($id);
                $book->update($data);
                $book->authors()->sync($data['author_ids']);

                return $book;
            });
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
