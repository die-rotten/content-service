<?php

namespace App\Services;

use App\Book;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class MyBookService
{
    /**
     * Read find book and update last read at attribute.
     *
     * @param int $user_id
     * @param int $book_id
     * @return void
     */
    public function find($user_id, $book_id)
    {
        try {
            $book = Book::findOrFail($book_id);

            DB::table('books_users')
                ->updateOrInsert(
                    [
                        'book_id' => $book_id,
                        'user_id' => $user_id
                    ],
                    [
                        'last_read_at' => Carbon::now(),
                    ]
                );

            return $book;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Read return book content.
     *
     * @param int $user_id
     * @param int $book_id
     * @param int $content_id
     * @return void
     */
    public function read($user_id, $book_id, $content_id)
    {
        try {
            $book = $this->find($user_id, $book_id);

            return $book->contents()->find($content_id);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * GetLatest return latest read and limit the return.
     *
     * @param int $user_id
     * @param int $limit
     * @return void
     */
    public function getLatest($user_id, $limit)
    {
        $books_user = DB::table('books_users')
            ->where('user_id', '=', $user_id)
            ->whereNotNull('last_read_at')
            ->orderByDesc('last_read_at')
            ->limit($limit)
            ->get();

        $book = Book::whereIn('id', $books_user->pluck('book_id')->toArray())
            ->take($limit)
            ->get();

        $books_user_map = collect($books_user)->map(function ($item) use ($book) {
            $book = $book->find($item->book_id);
            $item->book = $book;

            return $item;
        });

        return $books_user_map;
    }
}
