<?php

namespace App\Services;

use App\Publisher;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PublisherService
{
    /**
     * Return all publisher.
     *
     * @return void
     */
    public function get()
    {
        return Publisher::get();
    }

    /**
     * Find a publisher.
     *
     * @param int $id
     * @return void
     */
    public function find($id)
    {
        return Publisher::findOrFail($id);
    }

    /**
     * Create a new publisher.
     *
     * @param array $data
     * @return void
     */
    public function create($data)
    {
        try {
            $publisher = Publisher::create($data);

            return $publisher;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Update a publisher.
     *
     * @param int $id
     * @param array $data
     * @return void
     */
    public function update($id, $data)
    {
        try {
            $publisher = $this->find($id);
            $publisher->update($data);

            return $publisher->refresh();
        } catch (ModelNotFoundException $e) {
            throw $e;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Delete will delete a publishers.
     *
     * @param id $id
     * @return void
     */
    public function delete($id)
    {
        try {
            $publisher = $this->find($id);
            $publisher->delete();

            return true;
        } catch (ModelNotFoundException $e) {
            throw $e;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
