<?php

namespace App\Services;

use App\Author;
use App\Events\AuthorCreated;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AuthorService
{
    /**
     * Return all author.
     *
     * @return void
     */
    public function get()
    {
        return Author::with('publisher')->get();
    }

    /**
     * Return an author
     *
     * @param int $id
     * @return void
     */
    public function find($id)
    {
        return Author::findOrFail($id);
    }

    /**
     * Create creates new author.
     *
     * @param array $data
     * @return void
     */
    public function create($data)
    {
        try {
            $author = Author::create($data);

            event(new AuthorCreated($author));

            return $author;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Update an author.
     *
     * @param int $id
     * @param array $data
     * @return void
     */
    public function update($id, $data)
    {
        try {
            $author = $this->find($id);
            $author->update($data);

            return $author->refresh();
        } catch (ModelNotFoundException $e) {
            throw $e;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Delete will delete an author.
     *
     * @param id $id
     * @return void
     */
    public function delete($id)
    {
        try {
            $author = $this->find($id);
            $author->delete();

            return true;
        } catch (ModelNotFoundException $e) {
            throw $e;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
