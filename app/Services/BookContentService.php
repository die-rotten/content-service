<?php

namespace App\Services;

use App\BookContent;

class BookContentService
{
    /**
     * Find return a book content.
     *
     * @param int $id
     * @return void
     */
    public function find($id)
    {
        return BookContent::findOrFail($id);
    }

    /**
     * Create store book content.
     *
     * @param array $data
     * @return void
     */
    public function create(array $data)
    {
        return BookContent::create($data);
    }

    /**
     * Update updates book content.
     *
     * @param int $id
     * @param array $data
     * @return void
     */
    public function update($id, array $data)
    {
        $content = BookContent::findOrFail($id);
        $content->update($data);

        return $content;
    }
}
