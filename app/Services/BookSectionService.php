<?php

namespace App\Services;

use App\BookSection;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class BookSectionService
{
    /**
     * Get return all book sections.
     *
     * @return void
     */
    public function get()
    {
        return BookSection::with(['contents'])
            ->orderBy('id')
            ->get();
    }

    public function find($id)
    {
        return BookSection::with(['contents'])->find($id);
    }

    /**
     * Create store a book section.
     *
     * @param array $data
     * @return void
     */
    public function create(array $data)
    {
        return BookSection::create($data);
    }

    /**
     * Update a book section.
     *
     * @param int $id
     * @param array $data
     * @return void
     */
    public function update($id, array $data)
    {
        try {
            $section = BookSection::findOrFail($id);
            $section->update($data);

            return $section->refresh();
        } catch (ModelNotFoundException $e) {
            throw $e;
        }
    }
}
