<?php

namespace App\Services;

use App\Package;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class PackageService
{
    /**
     * Get return all package.
     *
     * @return void
     */
    public function get($query = [])
    {
        if (empty($query)) {
            return Package::with(['books'])->get();
        }

        return Package::with(['books'])
                    ->whereIn('id', $query)
                    ->get();
    }

    /**
     * GetByUser return all packages by userID.
     *
     * @param int $user_id
     * @return void
     */
    public function getByUser($user_id)
    {
        $packageIds = DB::table('packages_users')
                        ->where('user_id', '=', $user_id)
                        ->get()
                        ->pluck('package_id');

        $packages = Package::with(['books'])
                        ->whereIn('id', $packageIds->toArray())
                        ->get();

        return $packages;
    }

    /**
     * Return all last read package by user.
     *
     * @param int $user_id
     * @param int $limit
     * @return void
     */
    public function getLastRead($user_id, $limit)
    {
        $user_packages = DB::table('packages_users')
                        ->where('user_id', '=', $user_id)
                        ->whereNotNull('last_read_at')
                        ->orderByDesc('last_read_at')
                        ->limit($limit)
                        ->get();

        $packages = Package::whereIn('id', $user_packages->pluck('package_id')->toArray())
                        ->take($limit)
                        ->get();

        $package_user_map = collect($user_packages)->map(function ($item) use ($packages) {
            $package = $packages->find($item->package_id);
            $item->package = $package;

            return $item;
        });
        
        return $package_user_map;
    }

    /**
     * FindByUser finding a package by a user and update last read at.
     *
     * @param int $package_id
     * @param int $user_id
     * @return void
     */
    public function findByUser($package_id, $user_id)
    {
        $package = $this->find($package_id);

        DB::table('packages_users')
            ->where([
                ['user_id', '=', $user_id],
                ['package_id', '=', $package_id],
            ])
            ->update([
                'last_read_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);

        return $package;
    }

    /**
     * Validating package is already exist.
     *
     * @param int $package_id
     * @param int $user_id
     * @return void
     */
    public function validatePackage($package_id, $user_id)
    {
        $found = DB::table('packages_users')
                    ->where([
                        ['user_id', '=', $user_id],
                        ['package_id', '=', $package_id],
                    ])
                    ->first();

        if (is_null($found)) {
            return false;
        }

        return true;
    }

    /**
     * Find return a package
     *
     * @param int $id
     * @return void
     */
    public function find($id)
    {
        return Package::with(['books'])->findOrFail($id);
    }
    
    /**
     * Create creates a package.
     *
     * @param array $data
     * @return void
     */
    public function create($data)
    {
        $package = Package::create($data);
        $package->books()->sync($data['book_ids']);

        return $package;
    }

    /**
     * Update updates a package.
     *
     * @param int $id
     * @param array $data
     * @return void
     */
    public function update($id, $data)
    {
        $package = Package::find($id);
        $package->update($data);
        $package->books()->sync($data['book_ids']);

        return $package->refresh();
    }

    /**
     * Store user package is storing package_ids and user_id.
     *
     * @param int $user_id
     * @param array $package_ids
     * @return void
     */
    public function storeUserPackage($user_id, $package_ids)
    {
        try {
            $packages = [];

            foreach ($package_ids as $id) {
                array_push($packages, [
                    'package_id' => $id,
                    'user_id' => $user_id,
                    'last_read_at' => null,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]);
            }

            DB::table('packages_users')->insert($packages);

            return true;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
