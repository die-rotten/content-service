<?php

namespace App\Services;

use App\Author;
use App\Package;
use Illuminate\Support\Facades\DB;

class ReportService
{
    /**
     * Return amount of registered author.
     *
     * @return void
     */
    public function getAuthorsAmount()
    {
        try {
            $amount = Author::get()->count();

            return $amount;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Return amount of package
     *
     * @return void
     */
    public function getPackagesAmount()
    {
        try {
            $amount = Package::get()->count();

            return $amount;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function getMostPopularPackage($limit = 10)
    {
        try {
            $reports = DB::table('packages_users')
                        ->select(DB::raw('count(packages_users.package_id), packages.title'))
                        ->join('packages', 'packages.id', '=', 'packages_users.package_id')
                        ->groupBy(DB::raw('packages_users.package_id, packages.title'))
                        ->orderByDesc(DB::raw('count(packages_users.package_id)'))
                        ->limit($limit)
                        ->get();
            
            return $reports->all();
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
