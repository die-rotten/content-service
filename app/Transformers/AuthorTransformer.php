<?php

namespace App\Transformers;

use App\Author;
use League\Fractal\TransformerAbstract;

class AuthorTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'publisher',
    ];

    public function transform(Author $author)
    {
        return [
            'id' => $author->id,
            'firstName' => $author->first_name,
            'lastName' => $author->last_name,
            'title' => $author->title,
            'phone' => $author->phone,
            'email' => $author->email,
            'status' => $author->status,
        ];
    }

    /**
     * Include publisher model.
     *
     * @param Author $author
     * @return void
     */
    public function includePublisher(Author $author)
    {
        return $this->item($author->publisher, new PublisherTransformer);
    }
}
