<?php

namespace App\Transformers;

use App\BookContent;
use League\Fractal\TransformerAbstract;

class BookContentTransformer extends TransformerAbstract
{
    public function transform(BookContent $book_content)
    {
        return [
            'id' => $book_content->id,
            'sectionId' => $book_content->book_section_id,
            'title' => $book_content->title,
            'content' => $book_content->content,
            'type' => $book_content->type,
            'duration' => $book_content->duration,
            'durationUnit' => $book_content->duration_unit,
        ];
    }
}
