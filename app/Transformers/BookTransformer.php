<?php

namespace App\Transformers;

use App\Book;
use League\Fractal\TransformerAbstract;

class BookTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'sections',
        'authors',
    ];

    /**
     * Transform.
     *
     * @param Book $book
     * @return void
     */
    public function transform(Book $book)
    {
        $photo = !is_null($book->photo) ? env('CDN_ENDPOINT') . $book->photo : null;

        return [
            'id' => $book->id,
            'title' => $book->title,
            'description' => $book->description,
            'price' => $book->price,
            'photo' => $photo,
        ];
    }

    /**
     * Include BookSection model on transform response.
     *
     * @param Book $book
     * @return void
     */
    public function includeSections(Book $book)
    {
        return $this->collection($book->sections, new BookSectionTransformer);
    }

    /**
     * Include Author model on transform response.
     *
     * @param Book $book
     * @return void
     */
    public function includeAuthors(Book $book)
    {
        return $this->collection($book->authors, new AuthorTransformer);
    }
}
