<?php

namespace App\Transformers;

use App\Question;
use League\Fractal\TransformerAbstract;

class QuestionTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'answers',
        'content',
    ];

    /**
     * Transform.
     *
     * @param Question $question
     * @return void
     */
    public function transform(Question $question)
    {
        return [
            'id' => $question->id,
            'userId' => $question->user_id,
            'title' => $question->title,
            'description' => $question->description,
            'createdAt' => $question->created_at,
            'updatedAt' => $question->updated_at,
        ];
    }

    /**
     * Include Answer model in response.
     *
     * @param Question $question
     * @return void
     */
    public function includeAnswers(Question $question)
    {
        return $this->collection($question->answers, new AnswerTransformer);
    }

    /**
     * Include BookContent model in response.
     *
     * @param Question $question
     * @return void
     */
    public function includeContent(Question $question)
    {
        return $this->item($question->content, new BookContentTransformer);
    }
}
