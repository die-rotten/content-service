<?php

namespace App\Transformers;

use App\Publisher;
use League\Fractal\TransformerAbstract;

class PublisherTransformer extends TransformerAbstract
{
    public function transform(Publisher $publisher)
    {
        return [
            'id' => $publisher->id,
            'name' => $publisher->name,
            'address' => $publisher->address,
            'phone' => $publisher->phone,
            'logoUrl' => $publisher->logo_url,
        ];
    }
}
