<?php

namespace App\Transformers;

use App\Answer;
use League\Fractal\TransformerAbstract;

class AnswerTransformer extends TransformerAbstract
{
    /**
     * Transform.
     *
     * @param Answer $answer
     * @return void
     */
    public function transform(Answer $answer)
    {
        return [
            'id' => $answer->id,
            'userId' => $answer->user_id,
            'title' => $answer->title,
            'description' => $answer->description,
            'createdAt' => $answer->created_at,
            'updatedAt' => $answer->updated_at,
        ];
    }
}
