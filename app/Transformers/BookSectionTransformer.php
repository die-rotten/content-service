<?php

namespace App\Transformers;

use App\BookSection;
use League\Fractal\TransformerAbstract;

class BookSectionTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'contents',
    ];
    
    /**
     * Transform book section model on response.
     *
     * @param BookSection $book_section
     * @return void
     */
    public function transform(BookSection $book_section)
    {
        return [
            'id' => $book_section->id,
            'title' => $book_section->title,
            'description' => $book_section->description,
        ];
    }

    /**
     * Transform book content on transform response.
     *
     * @param BookSection $book_section
     * @return void
     */
    public function includeContents(BookSection $book_section)
    {
        return $this->collection($book_section->contents, new BookContentTransformer);
    }
}
