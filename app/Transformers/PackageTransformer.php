<?php

namespace App\Transformers;

use App\Package;
use League\Fractal\TransformerAbstract;

class PackageTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'books',
    ];

    public function transform(Package $package)
    {
        $photo = !is_null($package->cover_url) ? env('CDN_ENDPOINT') . $package->cover_url : null;

        return [
            'id' => $package->id,
            'title' => $package->title,
            'description' => $package->description,
            'photo' => $photo,
            'price' => $package->price,
            'discount' => $package->discount,
        ];
    }

    public function includeBooks(Package $package)
    {
        return $this->collection($package->books, new BookTransformer);
    }
}
