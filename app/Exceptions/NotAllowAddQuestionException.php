<?php

namespace App\Exceptions;

class NotAllowAddQuestionException extends \Exception
{
    protected $message = 'you are not allowed to add question';
}
