<?php

namespace App\Exceptions;

class AuthorNotAllowedException extends \Exception
{
    protected $message = 'author is not author of book';
}
