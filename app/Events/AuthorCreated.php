<?php

namespace App\Events;

use App\Author;

class AuthorCreated extends Event
{
    public $author;

    /**
     * AuthorCreated construct.
     *
     * @param Author $author
     */
    public function __construct(Author $author)
    {
        $this->author = $author;
    }
}
