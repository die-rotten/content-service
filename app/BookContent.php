<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookContent extends Model
{
    /**
     * Fillable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'book_section_id',
        'title',
        'content',
        'type',
        'duration',
        'duration_unit',
    ];

    /**
     * BelongsTo relation with BookSection model.
     *
     * @return void
     */
    public function section()
    {
        return $this->belongsTo(BookSection::class, 'book_section_id');
    }
}
