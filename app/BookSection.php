<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookSection extends Model
{
    /**
     * Fillable attribute.
     *
     * @var array
     */
    protected $fillable = [
        'book_id',
        'title',
        'description',
    ];

    /**
     * BelongsTo relation with book model.
     *
     * @return void
     */
    public function book()
    {
        return $this->belongsTo(Book::class, 'book_id');
    }

    /**
     * BelongsTo relation with BookContent model.
     *
     * @return void
     */
    public function contents()
    {
        return $this->hasMany(BookContent::class);
    }
}
