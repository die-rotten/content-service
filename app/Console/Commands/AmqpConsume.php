<?php

namespace App\Console\Commands;

use App\Publisher;
use App\Services\PackageService;
use Bschmitt\Amqp\Facades\Amqp;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class AmqpConsume extends Command
{
    /**
     * Signature console command.
     *
     * @var string
     */
    protected $signature = 'amqp:consume';

    /**
     * Description of command.
     *
     * @var string
     */
    protected $description = 'Start consuming AMQP message';

    /**
     * Create new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle(PackageService $package_service)
    {
        Amqp::consume('content_service_queue', function ($message, $resolver) use ($package_service) {
            $data = json_decode($message->body, true);
            
            $is_store = $package_service->storeUserPackage(
                $data['user_id'],
                $data['package_ids']
            );

            if ($is_store) {
                $resolver->acknowledge($message);
                echo 'Message has been acknowledged';
            }
        });
    }
}
