<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    /**
     * Fillable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'content_id',
        'user_id',
        'title',
        'description'
    ];

    public function content()
    {
        return $this->belongsTo(BookContent::class, 'content_id');
    }

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }
}
