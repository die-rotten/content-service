<?php

namespace App\Http\Controllers;

use App\Services\MyBookService;
use App\Transformers\BookContentTransformer;
use App\Transformers\BookTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class MyBookController extends RestController
{
    /**
     * @var BookTransformer
     */
    protected $transformer = BookTransformer::class;

    /**
     * @var MyBookService
     */
    protected $my_book_service;

    /**
     * MyBookController construct.
     *
     * @param MyBookService $my_book_service
     */
    public function __construct(MyBookService $my_book_service)
    {
        parent::__construct();
        $this->my_book_service = $my_book_service;
    }

    /**
     * Find return book and store its data.
     *
     * @param Request $request
     * @param int $id
     * @return void
     */
    public function find(Request $request, $id)
    {
        try {
            $book = $this->my_book_service->find($request->user_id, $id);
            $response = $this->generateItem($book);

            return $this->sendResponse($response);
        } catch (ModelNotFoundException $e) {
            return $this->sendNotFoundErrorResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    /**
     * Read return book content and store its data.
     *
     * @param Request $request
     * @param int $id
     * @return void
     */
    public function read(Request $request, $id, $content_id)
    {
        try {
            $content = $this->my_book_service->read($request->user_id, $id, $content_id);
            $this->transformer = BookContentTransformer::class;
            $response = $this->generateItem($content);

            return $this->sendResponse($response);
        } catch (ModelNotFoundException $e) {
            return $this->sendNotFoundErrorResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    /**
     * Get latest return latest read books.
     *
     * @param Request $request
     * @return void
     */
    public function getLatest(Request $request)
    {
        $limit = $request->query('limit') ?: 10;

        try {
            $books = $this->my_book_service->getLatest($request->user_id, $limit);

            return response()->json(['data' => $books]);
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }
}
