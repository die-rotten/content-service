<?php

namespace App\Http\Controllers;

use League\Fractal\Resource\Item;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection as ResourceCollection;
use League\Fractal\Resource\ResourceInterface;

class RestController extends Controller
{
    /**
     * Transformer Class of response.
     */
    protected $transformer;

    /**
     * @var Fractal/Manager
     */
    protected $manager;

    public function __construct()
    {
        $this->manager = new Manager;
    }

    /**
     * Generate collection data.
     *
     * @param $data
     * @return ResourceInterface
     */
    public function generateCollection($data)
    {
        if (isset($_GET['include'])) {
            $this->manager->parseIncludes($_GET['include']);
        }

        return new ResourceCollection($data, new $this->transformer);
    }

    /**
     * Generate item data.
     *
     * @param $data
     * @return ResourceInterface
     */
    public function generateItem($data)
    {
        if (isset($_GET['include'])) {
            $this->manager->parseIncludes($_GET['include']);
        }

        return new Item($data, new $this->transformer);
    }

    /**
     * Return response.
     *
     * @param ResourceInterface $resource
     * @param integer $status
     * @return void
     */
    public function sendResponse(ResourceInterface $resource, $status = 200)
    {
        return response()->json(
            $this->manager->createData($resource)->toArray(),
            $status
        );
    }

    /**
     * Send internal server error response.
     *
     * @param string $message
     * @return void
     */
    public function sendInternalErrorResponse($message)
    {
        return response()->json($message, 500);
    }

    /**
     * Send forbidden response.
     *
     * @param string $message
     * @return void
     */
    public function sendForbiddenResponse($message)
    {
        return response()->json($message, 403);
    }

    /**
     * Send not found error response
     *
     * @param string $message
     * @return void
     */
    public function sendNotFoundErrorResponse($message)
    {
        return response()->json($message, 404);
    }
}
