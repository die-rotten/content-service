<?php

namespace App\Http\Controllers;

use App\Exceptions\AuthorNotAllowedException;
use App\Transformers\AnswerTransformer;
use App\Services\AnswerService;
use Illuminate\Http\Request;

class AnswerController extends RestController
{
    protected $transformer = AnswerTransformer::class;

    protected $answer_service;

    public function __construct(AnswerService $answer_service)
    {
        parent::__construct();
        $this->answer_service = $answer_service;
    }

    /**
     * Store stores new answer.
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'questionId' => 'required',
            'title' => 'required',
            'description' => 'required',
        ]);

        try {
            return $this->sendResponse($this->generateItem(
                $this->answer_service->create([
                    'question_id' => $request->questionId,
                    'user_id' => $request->user['id'],
                    'email' => $request->user['email'],
                    'title' => $request->title,
                    'description' => $request->description,
                ]),
                201
            ));
        } catch (AuthorNotAllowedException $e) {
            return $this->sendForbiddenResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }
}
