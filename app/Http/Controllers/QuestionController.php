<?php

namespace App\Http\Controllers;

use App\Exceptions\NotAllowAddQuestionException;
use App\Services\QuestionService;
use App\Transformers\QuestionTransformer;
use Illuminate\Http\Request;

class QuestionController extends RestController
{
    protected $transformer = QuestionTransformer::class;

    protected $question_service;

    public function __construct(QuestionService $question_service)
    {
        parent::__construct();

        $this->question_service = $question_service;
    }

    /**
     * Get return all questions by content and user.
     *
     * @param Request $request
     * @param int $content_id
     * @return void
     */
    public function get(Request $request, $content_id)
    {
        try {
            $questions = $this->question_service->getByContentAndUser($content_id, $request->user_id);

            return $this->sendResponse(
                $this->generateCollection($questions)
            );
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    /**
     * Get for author return all questions for an author.
     *
     * @param Request $request
     * @return void
     */
    public function getForAuthor(Request $request)
    {
        try {
            $questions = $this->question_service->getForAuthor($request->user);

            return $this->sendResponse(
                $this->generateCollection($questions)
            );
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    /**
     * Store stores new question in database.
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'contentId' => 'required',
            'title' => 'required',
            'description' => 'required',
        ]);

        try {
            return $this->sendResponse(
                $this->generateItem(($this->question_service->create([
                    'user_id' => $request->user_id,
                    'content_id' => $request->contentId,
                    'title' => $request->title,
                    'description' => $request->description,
                ]))),
                201
            );
        } catch (NotAllowAddQuestionException $e) {
            return $this->sendForbiddenResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }
}
