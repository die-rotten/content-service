<?php

namespace App\Http\Controllers;

use App\Transformers\AuthorTransformer;
use App\Services\AuthorService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class AuthorController extends RestController
{
    protected $transformer = AuthorTransformer::class;

    /**
     * @var AuthorService
     */
    protected $author_service;

    public function __construct(AuthorService $author_service)
    {
        parent::__construct();

        $this->author_service = $author_service;
    }
    
    /**
     * Get return all authors.
     *
     * @return void
     */
    public function get()
    {
        try {
            $resources = $this->author_service->get();
            $data = $this->generateCollection($resources);

            return $this->sendResponse($data);
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    /**
     * Find return an author.
     *
     * @param int $id
     * @return void
     */
    public function find($id)
    {
        try {
            $resource = $this->author_service->find($id);
            $data = $this->generateItem($resource);

            return $this->sendResponse($data);
        } catch (ModelNotFoundException $e) {
            return $this->sendNotFoundErrorResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    /**
     * Store stores a new author.
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'publisherId' => 'required',
            'firstName' => 'required',
            'lastName' => 'required',
            'phone' => 'required',
            'email' => 'required|unique:authors',
            'title' => 'required'
        ]);

        try {
            $data = $this->author_service->create([
                'first_name' => $request->firstName,
                'last_name' => $request->lastName,
                'publisher_id' => $request->publisherId,
                'phone' => $request->phone,
                'email' => $request->email,
                'title' => $request->title,
                'status' => true,
            ]);

            $item = $this->generateItem($data);

            return $this->sendResponse($item, 201);
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    /**
     * Update updates a new author.
     *
     * @param Request $request
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'publisherId' => 'required',
            'firstName' => 'required',
            'lastName' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'title' => 'required'
        ]);

        try {
            $data = $this->author_service->update($id, [
                'first_name' => $request->firstName,
                'last_name' => $request->lastName,
                'publisher_id' => $request->publisherId,
                'phone' => $request->phone,
                'email' => $request->email,
                'title' => $request->title,
                'status' => true,
            ]);

            $item = $this->generateItem($data);

            return $this->sendResponse($item);
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    /**
     * Delete will delete an author.
     *
     * @param int $id
     * @return void
     */
    public function delete($id)
    {
        try {
            $this->author_service->delete($id);

            return response()->json();
        } catch (ModelNotFoundException $e) {
            return $this->sendNotFoundErrorResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }
}
