<?php

namespace App\Http\Controllers;

use App\Services\BookContentService;
use App\Transformers\BookContentTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class BookContentController extends RestController
{
    protected $transformer = BookContentTransformer::class;

    /**
     * @var BookContentService
     */
    protected $book_content_service;

    public function __construct(BookContentService $book_content_service)
    {
        parent::__construct();
        $this->book_content_service = $book_content_service;
    }

    /**
     * Find finds a book content.
     *
     * @param int $id
     * @return void
     */
    public function find($id)
    {
        try {
            return $this->sendResponse(
                $this->generateItem($this->book_content_service->find($id))
            );
        } catch (ModelNotFoundException $e) {
            return $this->sendNotFoundErrorResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    /**
     * Store stores book content.
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'sectionId' => 'required',
            'title' => 'required',
            'content' => 'required',
            'type' => 'required',
            'duration' => 'required',
            'durationUnit' => 'required',
        ]);

        try {
            $data = [
                'book_section_id' => $request->sectionId,
                'title' => $request->title,
                'content' => json_encode(['content' => $request->content]),
                'type' => $request->type,
                'duration' => $request->duration,
                'duration_unit' => $request->durationUnit,
            ];

            return $this->sendResponse(
                $this->generateItem($this->book_content_service->create($data)),
                201
            );
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'sectionId' => 'required',
            'title' => 'required',
            'content' => 'required',
            'type' => 'required',
            'duration' => 'required',
            'durationUnit' => 'required',
        ]);

        try {
            $data = [
                'book_section_id' => $request->sectionId,
                'title' => $request->title,
                'content' => json_encode(['content' => $request->content]),
                'type' => $request->type,
                'duration' => $request->duration,
                'duration_unit' => $request->durationUnit,
            ];

            return $this->sendResponse(
                $this->generateItem($this->book_content_service->update($id, $data))
            );
        } catch (ModelNotFoundException $e) {
            return $this->sendNotFoundErrorResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }
}
