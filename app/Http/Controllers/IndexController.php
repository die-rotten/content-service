<?php

namespace App\Http\Controllers;

class IndexController extends Controller
{
    public function index()
    {
        return response()->json([
            'version' => '1.0.0',
            'name' => 'Rotten/ContentService',
            'description' => 'Microservice that responsible to handle all content related',
        ]);
    }
}
