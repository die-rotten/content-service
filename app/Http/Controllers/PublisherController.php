<?php

namespace App\Http\Controllers;

use App\Services\PublisherService;
use App\Transformers\PublisherTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class PublisherController extends RestController
{
    protected $transformer = PublisherTransformer::class;

    /**
     * @var PublisherService
     */
    protected $publisher_service;

    public function __construct(PublisherService $publisher_service)
    {
        parent::__construct();

        $this->publisher_service = $publisher_service;
    }

    /**
     * Get return all publisher.
     *
     * @return void
     */
    public function get()
    {
        try {
            $resources = $this->publisher_service->get();
            $data = $this->generateCollection($resources);

            return $this->sendResponse($data);
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    /**
     * Find will return a publisher.
     *
     * @param int $id
     * @return void
     */
    public function find($id)
    {
        try {
            $resource = $this->publisher_service->find($id);
            $data = $this->generateItem($resource);

            return $this->sendResponse($data);
        } catch (ModelNotFoundException $e) {
            return $this->sendNotFoundErrorResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    /**
     * Store will store new publisher.
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required',
            'address' => 'required',
        ]);

        try {
            $data = $this->publisher_service->create([
                'name' => $request->name,
                'phone' => $request->phone,
                'address' => $request->address,
            ]);

            $item = $this->generateItem($data);

            return $this->sendResponse($item, 201);
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    /**
     * Update will update a publisher data.
     *
     * @param int $id
     * @param Request $request
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required',
            'address' => 'required',
        ]);

        try {
            $data = $this->publisher_service->update($id, [
                'name' => $request->name,
                'phone' => $request->phone,
                'address' => $request->address,
            ]);

            $item = $this->generateItem($data);

            return $this->sendResponse($item);
        } catch (ModelNotFoundException $e) {
            return $this->sendNotFoundErrorResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    /**
     * Delete will delete a publisher.
     *
     * @param int $id
     * @return void
     */
    public function delete($id)
    {
        try {
            $this->publisher_service->delete($id);

            return response()->json();
        } catch (ModelNotFoundException $e) {
            return $this->sendNotFoundErrorResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }
}
