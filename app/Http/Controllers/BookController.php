<?php

namespace App\Http\Controllers;

use App\Services\BookService;
use App\Transformers\BookSectionTransformer;
use App\Transformers\BookTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class BookController extends RestController
{
    protected $transformer = BookTransformer::class;

    /**
     * @var BookService
     */
    protected $book_service;

    public function __construct(BookService $book_service)
    {
        parent::__construct();

        $this->book_service = $book_service;
    }

    /**
     * Get return all books
     *
     * @return void
     */
    public function get()
    {
        try {
            $data = $this->book_service->get();
            $resources = $this->generateCollection($data);

            return $this->sendResponse($resources);
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    /**
     * Return book sections by a book.
     *
     * @param int $id
     * @return void
     */
    public function getSections($id)
    {
        try {
            $this->transformer = BookSectionTransformer::class;
            $sections = $this->book_service->getSections($id);
            $response = $this->generateCollection($sections);

            return $this->sendResponse($response);
        } catch (ModelNotFoundException $e) {
            return $this->sendNotFoundErrorResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    /**
     * Return all books by authenticated author.
     *
     * @param Request $request
     * @return void
     */
    public function getByAuthor(Request $request)
    {
        try {
            $books = $this->book_service->getByAuthor($request->user);
            $response = $this->generateCollection($books);

            return $this->sendResponse($response);
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    /**
     * Find return a book.
     *
     * @param int $id
     * @return void
     */
    public function find($id)
    {
        try {
            return $this->sendResponse(
                $this->generateItem($this->book_service->find($id))
            );
        } catch (ModelNotFoundException $e) {
            return $this->sendNotFoundErrorResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    /**
     * Create create a new book.
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'price' => 'required',
            'authorIds' => 'required',
            'photo' => 'required',
        ]);

        try {
            return $this->sendResponse(
                $this->generateItem($this->book_service->create([
                    'title' => $request->title,
                    'description' => $request->description,
                    'price' => $request->price,
                    'author_ids' => $request->authorIds,
                    'photo' => $request->photo,
                ])),
                201
            );
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    /**
     * Update updates a book.
     *
     * @param int $id
     * @param Request $request
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'price' => 'required',
            'authorIds' => 'required',
        ]);

        try {
            return $this->sendResponse(
                $this->generateItem($this->book_service->update($id, [
                    'title' => $request->title,
                    'description' => $request->description,
                    'price' => $request->price,
                    'author_ids' => $request->authorIds,
                ]))
            );
        } catch (ModelNotFoundException $e) {
            return $this->sendNotFoundErrorResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }
}
