<?php

namespace App\Http\Controllers;

use App\Transformers\BookSectionTransformer;
use App\Services\BookSectionService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class BookSectionController extends RestController
{
    protected $transformer = BookSectionTransformer::class;

    /**
     * @var BookSectionService
     */
    protected $book_section_service;

    public function __construct(BookSectionService $book_section_service)
    {
        parent::__construct();
        $this->book_section_service = $book_section_service;
    }

    /**
     * Get return all book sections.
     *
     * @return void
     */
    public function get()
    {
        try {
            return $this->sendResponse($this->generateCollection(
                $this->book_section_service->get()
            ));
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    /**
     * Find return a book section
     *
     * @param int $id
     * @return void
     */
    public function find($id)
    {
        try {
            return $this->sendResponse($this->generateItem(
                $this->book_section_service->find($id)
            ));
        } catch (ModelNotFoundException $e) {
            return $this->sendNotFoundErrorResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    /**
     * Store stores book section.
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'bookId' => 'required',
            'title' => 'required',
            'description' => 'required',
        ]);

        try {
            return $this->sendResponse(
                $this->generateItem($this->book_section_service->create([
                    'book_id' => $request->bookId,
                    'title' => $request->title,
                    'description' => $request->description,
                ])),
                201
            );
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    /**
     * Update updates book section.
     *
     * @param int $id
     * @param Request $request
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'bookId' => 'required',
            'title' => 'required',
            'description' => 'required',
        ]);

        try {
            return $this->sendResponse(
                $this->generateItem($this->book_section_service->update($id, [
                    'book_id' => $request->bookId,
                    'title' => $request->title,
                    'description' => $request->description,
                ]))
            );
        } catch (ModelNotFoundException $e) {
            return $this->sendNotFoundErrorResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }
}
