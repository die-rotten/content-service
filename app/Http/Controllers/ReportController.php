<?php

namespace App\Http\Controllers;

use App\Services\ReportService;
use Illuminate\Http\Request;

class ReportController
{
    /**
     * @var ReportService
     */
    protected $report_service;

    /**
     * Construct of ReportController.
     *
     * @param ReportService $report_service
     */
    public function __construct(ReportService $report_service)
    {
        $this->report_service = $report_service;
    }

    /**
     * Get authors amount return amount of authors.
     *
     * @return void
     */
    public function getAuthorsAmount()
    {
        try {
            $amount = $this->report_service->getAuthorsAmount();

            return response()->json([
                'data' => [
                    'amount' => $amount,
                ],
            ]);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Get packages amount return amount of packages.
     *
     * @return void
     */
    public function getPackagesAmount()
    {
        try {
            $amount = $this->report_service->getPackagesAmount();

            return response()->json([
                'data' => [
                    'amount' => $amount,
                ],
            ]);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * GetMostPopularPackage return most popular package in all time.
     *
     * @return void
     */
    public function getMostPopularPackages(Request $request)
    {
        $default_limit = 10;

        try {
            $limit = $request->query('limit');

            if (!is_null($limit)) {
                $default_limit = $limit;
            }

            $reports = $this->report_service->getMostPopularPackage($default_limit);

            return response()->json([
                'data' => $reports,
            ]);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
}
