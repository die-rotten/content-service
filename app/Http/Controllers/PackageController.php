<?php

namespace App\Http\Controllers;

use App\Transformers\PackageTransformer;
use App\Services\PackageService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class PackageController extends RestController
{
    /**
     * @var PackageTransformer
     */
    protected $transformer = PackageTransformer::class;

    /**
     * @var PackageService
     */
    protected $package_service;

    /**
     * PackageController construct.
     *
     * @param PackageService $package_service
     */
    public function __construct(PackageService $package_service)
    {
        parent::__construct();
        
        $this->package_service = $package_service;
    }

    /**
     * Get return all packages.
     *
     * @param Request $request
     * @return void
     */
    public function get(Request $request)
    {
        $filters = [];

        try {
            if ($request->query('packageIds')) {
                $filters = explode(',', $request->query('packageIds'));
            }

            $data = $this->package_service->get($filters);
            $resources = $this->generateCollection($data);

            return $this->sendResponse($resources);
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    /**
     * GetMyPackage return all packages by a user.
     *
     * @param Request $request
     * @return void
     */
    public function getMyPackage(Request $request)
    {
        try {
            $data = $this->package_service->getByUser($request->user_id);
            $resources = $this->generateCollection($data);

            return $this->sendResponse($resources);
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    /**
     * GetLastRead return last read packages by a user.
     *
     * @param Request $request
     * @return void
     */
    public function getLastRead(Request $request)
    {
        $limit = $request->query('limit') ?: 10;

        try {
            $packages = $this->package_service->getLastRead($request->user_id, $limit);

            return response()->json([
                'data' => $packages,
            ]);
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    /**
     * FindMyPackage return package detail by a user.
     *
     * @param Request $request
     * @param [type] $id
     * @return void
     */
    public function findMyPackage(Request $request, $id)
    {
        try {
            $data = $this->package_service->findByUser($id, $request->user_id);
            $resources = $this->generateItem($data);

            return $this->sendResponse($resources);
        } catch (ModelNotFoundException $e) {
            return $this->sendNotFoundErrorResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    /**
     * Validate package means validating package if already bought by a user.
     *
     * @param Request $request
     * @param int $id
     * @return void
     */
    public function validatePackage(Request $request, $id)
    {
        try {
            $response = $this->package_service->validatePackage($id, $request->user_id);

            return response()->json($response);
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    /**
     * Find return package detail.
     *
     * @param int $id
     * @return void
     */
    public function find($id)
    {
        try {
            $data = $this->package_service->find($id);
            $resources = $this->generateItem($data);

            return $this->sendResponse($resources);
        } catch (ModelNotFoundException $e) {
            return $this->sendNotFoundErrorResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    /**
     * Store stores package and return it.
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'bookIds' => 'required',
            'price' => 'required',
            'discount' => 'required',
            'photo' => 'required',
        ]);

        try {
            return $this->sendResponse(
                $this->generateItem($this->package_service->create([
                    'title' => $request->title,
                    'description' => $request->description,
                    'book_ids' => $request->bookIds,
                    'price' => $request->price,
                    'discount' => $request->discount,
                    'cover_url' => $request->photo,
                ])),
                201
            );
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    /**
     * StoreUserPackage store information about package and user who owns it.
     *
     * @param Request $request
     * @return void
     */
    public function storeUserPackage(Request $request)
    {
        $this->validate($request, [
            'userId' => 'required',
            'packageIds' => 'required',
        ]);

        try {
            $this->package_service->storeUserPackage(
                $request->userId,
                $request->packageIds
            );

            return response()->json('created', 201);
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }

    /**
     * Update updates package and return it.
     *
     * @param int $id
     * @param Request $request
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'bookIds' => 'required',
            'price' => 'required',
            'discount' => 'required',
        ]);

        try {
            $data = [
                'title' => $request->title,
                'description' => $request->description,
                'book_ids' => $request->bookIds,
                'price' => $request->price,
                'discount' => $request->discount,
            ];

            $resources = $this->generateItem($this->package_service->update($id, $data));

            return $this->sendResponse($resources);
        } catch (ModelNotFoundException $e) {
            return $this->sendNotFoundErrorResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->sendInternalErrorResponse($e->getMessage());
        }
    }
}
