<?php

namespace App\Http\Middleware;

use Closure;
use Lcobucci\JWT\Parser;
use Illuminate\Http\Request;

class AuthMiddleware
{
    /**
     * @var JWT|Parser
     */
    protected $parser;

    /**
     * AuthMiddleware construct.
     *
     * @param Parser $parser
     */
    public function __construct(Parser $parser)
    {
        $this->parser = $parser;
    }

    /**
     * Handle authorization on request.
     *
     * @param Request $request
     * @param Closure $next
     * @return void
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $request->bearerToken();

        if (is_null($token) || empty($token)) {
            return response()->json('unauthorized', 401);
        }

        $user_id = (int) $this->parser->parse($token)->getClaim('sub');
        $request->user_id = $user_id;

        return $next($request);
    }
}
