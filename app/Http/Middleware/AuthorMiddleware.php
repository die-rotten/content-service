<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;
use Illuminate\Http\Request;

class AuthorMiddleware
{
    /**
     * @var GuzzleHttp\Client
     */
    protected $client;

    /**
     * AuthorMiddleware construct.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Handle handles author middleware.
     *
     * @param Request $request
     * @param Closure $next
     * @return void
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            $token = $request->bearerToken();
    
            if (is_null($token) || empty($token)) {
                return response()->json('unauthorized', 401);
            }
    
            $user = $this->handleRequest($token);
            $request->user = $user;
    
            return $next($request);
        } catch (Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Handle Request will perform http request to user_service
     * to fetch authenticated user.
     *
     * @param string $token
     * @return void
     */
    protected function handleRequest($token)
    {
        try {
            $url = env('USER_SERVICE_URL') . '/whoami';
            $response = $this->client->request('GET', $url, [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                ],
            ]);

            $body = (string) $response->getBody();
            $decoded = json_decode($body, true);

            return $decoded['data'];
        } catch (ServerException $e) {
            throw $e;
        } catch (RequestException $e) {
            throw $e;
        }
    }
}
