<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model
{
    use SoftDeletes;

    /**
     * Fillable attribute.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'price',
        'photo',
    ];

    /**
     * Cast variables
     *
     * @var array
     */
    protected $casts = [
        'price' => 'float',
    ];

    /**
     * HasMany relation with BookSection model.
     *
     * @return void
     */
    public function sections()
    {
        return $this->hasMany(BookSection::class, 'book_id');
    }

    /**
     * BelongsToMany relation with Author model.
     *
     * @return void
     */
    public function authors()
    {
        return $this->belongsToMany(Author::class, 'author_book');
    }

    /**
     * HasManyThrough relation with BookContent model.
     *
     * @return void
     */
    public function contents()
    {
        return $this->hasManyThrough(BookContent::class, BookSection::class);
    }

    /**
     * Many to many relation with package.
     *
     * @return void
     */
    public function packages()
    {
        return $this->belongsToMany(Package::class);
    }
}
