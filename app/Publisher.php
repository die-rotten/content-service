<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Publisher extends Model
{
    use SoftDeletes;

    /**
     * Fillable attribute.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'phone',
        'address',
    ];

    /**
     * Relation hasMany with author model.
     *
     * @return void
     */
    public function authors()
    {
        return $this->hasMany(Author::class, 'publisher_id');
    }
}
