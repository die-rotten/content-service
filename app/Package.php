<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $fillable = [
        'title',
        'description',
        'cover_url',
        'price',
        'discount',
    ];

    protected $casts = [
        'price' => 'float',
        'discount' => 'float',
    ];

    /**
     * Define many to many relation with book.
     *
     * @return void
     */
    public function books()
    {
        return $this->belongsToMany(Book::class);
    }
}
