<?php

namespace App\Listeners;

use App\Events\AuthorCreated;
use Bschmitt\Amqp\Facades\Amqp;

class ProcessCreatedAuthor
{
    public function __construct()
    {
        //
    }

    public function handle(AuthorCreated $event)
    {
        $data = [
            'email' => $event->author->email,
            'password' => env('DEFAULT_PASSWORD'),
            'personal' => [
                'full_name' => $event->author->first_name . ' ' . $event->author->last_name,
                'phone_number' => $event->author->phone,
            ],
        ];

        $message = json_encode($data);

        Amqp::publish(
            'user_service_queue',
            (string) $message,
            ['queue' => 'user_service_queue']
        );
    }
}
