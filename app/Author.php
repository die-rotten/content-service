<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Author extends Model
{
    use SoftDeletes;

    /**
     * Fillable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'publisher_id',
        'first_name',
        'last_name',
        'phone',
        'title',
        'email',
        'status',
    ];

    /**
     * Define belongsTo relation with Publisher.
     *
     * @return void
     */
    public function publisher()
    {
        return $this->belongsTo(Publisher::class);
    }

    /**
     * BelongsToMany relation with Book model.
     *
     * @return void
     */
    public function books()
    {
        return $this->belongsToMany(Book::class, 'author_book');
    }
}
