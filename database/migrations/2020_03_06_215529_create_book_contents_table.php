<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_contents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('book_section_id');
            $table->string('title');
            $table->jsonb('content');
            $table->string('type');
            $table->integer('duration');
            $table->string('duration_unit');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('book_section_id')->references('id')->on('book_sections');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_contents');
    }
}
