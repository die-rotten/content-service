<?php

use Illuminate\Database\Seeder;
use Laravel\Lumen\Testing\DatabaseMigrations;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PublishersTableSeeder::class);
        $this->call(AuthorsTableSeeder::class);
        $this->call(BooksTableSeeder::class);
        $this->call(PackagesTableSeeder::class);
        $this->call(QuestionsTableSeeder::class);
        $this->call(PackagesUsersTableSeeder::class);
    }
}
