<?php

use App\Author;
use Illuminate\Database\Seeder;

class AuthorsTableSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'publisher_id' => 1,
                'first_name' => 'Abdul',
                'last_name' => 'Khodir',
                'title' => 'Ustad',
                'phone' => '087789090000',
                'email' => 'ustabdulkhodir@gmail.com',
                'status' => true,
            ]
        ];

        foreach ($data as $item) {
            if (is_null(Author::find($item['id']))) {
                Author::create($item);
            }
        }
    }
}
