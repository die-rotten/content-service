<?php

use App\Book;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BooksTableSeeder extends Seeder
{
    public function run()
    {
        $data = $this->prepareData();

        DB::transaction(function () use ($data) {
            $book = Book::create([
                'title' => $data['title'],
                'description' => $data['description'],
                'price' => $data['price'],
                'photo' => $data['photo'],
            ]);

            $book->authors()->sync($data['authorIds']);
            
            collect($data['sections'])->each(function ($item) use ($book) {
                $book->sections()->create([
                    'title' => $item['title'],
                    'description' => $item['description'],
                ]);

                collect($item['contents'])->each(function ($item) use ($book) {
                    $section = $book->sections->first();
                    $section->contents()->create([
                        'title' => $item['title'],
                        'content' => json_encode($item['content']),
                        'type' => $item['type'],
                        'duration' => $item['duration'],
                        'duration_unit' => $item['duration_unit'],
                    ]);
                });
            });
        });
    }

    private function prepareData()
    {
        return [
            'id' => 1,
            'title' => 'Pengertian Hadist',
            'description' => 'Membahas pengertian - pengertian hadist',
            'price' => 50000,
            'photo' => null,
            'authorIds' => [1],
            'sections' => [
                [
                    'id' => 1,
                    'title' => 'Pengertian dan Penjelasan Jenis Hadist',
                    'description' => 'Memberikan gambaran jenis - jenis hadist',
                    'contents' => [
                        [
                            'id' => 1,
                            'title' => 'Definisi dan Pengertian Hadist',
                            'content' => ['content' => '<b> Pengertian Tentang Hadist </b>'],
                            'type' => 'Bacaan',
                            'duration' => 30,
                            'duration_unit' => 'Menit',
                        ],
                    ],
                ],
                [
                    'id' => 2,
                    'title' => 'Jenis - Jenis Hadist',
                    'description' => 'Pengelompokan jenis - jenis hadist',
                    'contents' => [
                        [
                            'id' => 2,
                            'title' => 'Jenis - Jenis Hadist',
                            'content' => ['content' => '<b> Jenis - jenis hadist sebagai berikut </b>'],
                            'type' => 'Bacaan',
                            'duration' => 30,
                            'duration_unit' => 'Menit',
                        ],
                    ],
                ],
            ],
        ];
    }
}
