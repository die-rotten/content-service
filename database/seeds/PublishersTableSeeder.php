<?php

use App\Publisher;
use Illuminate\Database\Seeder;

class PublishersTableSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'name' => 'PT Gramedia',
                'phone' => '021456789',
                'address' => 'Jln Pemalang No. 21',
            ]
        ];

        foreach ($data as $item) {
            if (is_null(Publisher::find($item['id']))) {
                Publisher::create($item);
            }
        }
    }
}
