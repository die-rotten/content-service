<?php

use App\Book;
use App\Package;
use Illuminate\Database\Seeder;

class PackagesTableSeeder extends Seeder
{
    public function run()
    {
        $data = [
            'id' => 1,
            'title' => 'Paket Hadist',
            'description' => 'Paket berisi buku - buku hadist',
            'price' => 10000,
            'discount' => 0,
            'bookIds' => [1],
        ];

        $package = Package::create([
            'id' => $data['id'],
            'title' => $data['title'],
            'description' => $data['description'],
            'price' => $data['price'],
            'discount' => $data['discount'],
        ]);

        $package->books()->sync($data['bookIds']);
    }
}
