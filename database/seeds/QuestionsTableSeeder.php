<?php

use App\Question;
use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    public function run()
    {
        $data = [
            'id' => 1,
            'content_id' => 1,
            'user_id' => 1,
            'title' => 'test',
            'description' => 'test',
        ];

        if (is_null(Question::find($data['id']))) {
            Question::create($data);
        }
    }
}
